import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'rinkrator-v5',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
