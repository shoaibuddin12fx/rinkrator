// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    // projectId: '<your-project-id>',
    // appId: '<your-app-id>',
    // measurementId: '<your-measurement-id>',
    apiKey: "AIzaSyD7jrox35II_8DvlbwcdF7wWGHssdumm3k",
    authDomain: "rink-rater-8f338.firebaseapp.com",
    databaseURL: "https://rink-rater-8f338.firebaseio.com",
    storageBucket: "rink-rater-8f338.appspot.com",
    messagingSenderId: "208065488663"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
