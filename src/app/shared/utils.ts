import { Injectable } from "@angular/core";
import { AlertController, LoadingController, Platform } from "ionic-angular";
import { Dialogs, SpinnerDialog } from "ionic-native";

@Injectable()
export class UtilsService {

    private loading: any;

    constructor(
        private alertController: AlertController,
        private loadingController: LoadingController,
        private platform: Platform
    ) {}

    private checkMessage(value: any): any {
        return typeof value != 'string' ? JSON.stringify(value) : value;
    }

    alert(message: any = 'Default alert', title: string = 'Rink Alert!', buttonName: string = 'Ok') {

        if (this.platform.is('cordova'))
            Dialogs.alert(this.checkMessage(message), title, buttonName);
        else {
            let alert = this.alertController.create({
                title: title,
                message: message,
                buttons: [buttonName]
            });
            alert.present();
        }
    }

    confirm(message: any = 'Default message', title: string = 'Rink Confirm!', buttonLabels: Array<string> = ['OK', 'Cancel']): Promise<number> {

        return Dialogs.confirm(this.checkMessage(message), title, buttonLabels);
        // else {
        //
        //     let alert = this.alertController.create({
        //         title: title,
        //         message: message,
        //         buttons: [
        //             {
        //                 text: buttonLabels[1],
        //                 handler: () => {
        //                     console.log('Disagree clicked');
        //                 }
        //             },
        //             {
        //                 text: buttonLabels[0],
        //                 handler: () => {
        //                     console.log('Agree clicked');
        //                 }
        //             }
        //         ]
        //     });
        //
        //     alert.present();
        // }
    }

    showLoading(message: any = 'Loading...', title: string = '') {

        if (this.platform.is('cordova')) {
            SpinnerDialog.show(title, this.checkMessage(message), true);
        } else {
            this.loading = this.loadingController.create({
                spinner: 'dots',
                content: message,
                dismissOnPageChange: true
            });
            this.loading.present();
        }
    }

    hideLoading() {

        if (this.platform.is('cordova'))
            SpinnerDialog.hide();
        else {
            this.loading.dismiss();
        }
    }
}
