const baseUrl: string = 'http://www.rinkrater.com';

export const BASE_URL: string = baseUrl;

export const TERMS_OF_USE: string = `${baseUrl}/terms-of-use/`;
export const PRIVACY_POLICY: string = `${baseUrl}/privacy-policy/`;
export const GEAR_STORE: string = `${baseUrl}/gear-store/`;
export const REF_HAND_SIGNALS: string = `${baseUrl}/hockey-ref-hand-signals/`;
export const FAQS: string = `${baseUrl}/faqs/`;
export const HOCKEY_RANKINGS_URL: string = 'http://myhockeyrankings.com/';
export const NORTH_POLE_URL: string = 'http://www.northpoledesign.com/';

export const IOS_MARKET: string = 'https://itunes.apple.com/us/app/rink-rater-hockey-rink-reviews-by-you/id1203876613?mt=8';
export const ANDROID_MARKET: string = 'https://play.google.com/store/apps/details?id=com.northpoledesign.rinkrater&hl=en';

export const FACEBOOK_SOCIAL: string = 'https://www.facebook.com/rinkrater/';
export const TWITTER_SOCIAL: string = 'https://twitter.com/rinkrater/';
