export class Rink {

    constructor(
        public $key: string,
        public active: number,
        public address: string,
        public altname: string,
        public altname2: string,
        public city: string,
        public country: string,
        public created_at: number,
        public created_by: string,
        public description: string,
        public lat: number,
        public long: number,
        public name: string,
        public nbr: number,
        public phone: string,
        public state: string,
        public updated_at: string,
        public updated_by: string,
        public website: string,
        public zip: string
    ) {}

    static fromJson({
        $key,
        active,
        address,
        altname,
        altname2,
        city,
        country,
        created_at,
        created_by,
        description,
        lat,
        long,
        name,
        nbr,
        phone,
        state,
        updated_at,
        updated_by,
        website,
        zip
    }) {
        return new Rink(
            $key,
            active,
            address,
            altname,
            altname2,
            city,
            country,
            created_at,
            created_by,
            description,
            lat,
            long,
            name,
            nbr,
            phone,
            state,
            updated_at,
            updated_by,
            website,
            zip
        );
    }

    static fromJsonArray(json: any[]): Rink[] {
        return json.map(Rink.fromJson);
    }

}
