import { Component, ViewChild, Inject } from "@angular/core";
import { Platform } from "@ionic/angular";
import { GEAR_STORE, NORTH_POLE_URL, REF_HAND_SIGNALS } from "./shared/constants";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  rootPage: any;
  firebase: any;
  state: string = 'out';

  pages: Array<{ title: string, component: any }>;
  staggeringPages: Array<{ title: string, component: any }> = [];
  next: number = 0;

  public appPages = [
    { title: 'Inbox', url: '/folder/Inbox', icon: 'mail' },
    { title: 'Outbox', url: '/folder/Outbox', icon: 'paper-plane' },
    { title: 'Favorites', url: '/folder/Favorites', icon: 'heart' },
    { title: 'Archived', url: '/folder/Archived', icon: 'archive' },
    { title: 'Trash', url: '/folder/Trash', icon: 'trash' },
    { title: 'Spam', url: '/folder/Spam', icon: 'warning' },
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor(
    private platform: Platform,
  ) { }

  itemStateDone() {
    if (this.next < this.pages.length && this.state == 'in')
      this.staggeringPages.push(this.pages[this.next++]);
  }

  openPage(page) {
    if (page.title === 'RINK RATER PRO SHOP') {
      this.platform.ready().then(
        () => new InAppBrowser(GEAR_STORE, '_blank')
      );
    } else if (page.title === 'REFEREE HAND SIGNALS') {
      this.platform.ready().then(
        () => new InAppBrowser(REF_HAND_SIGNALS, '_blank')
      );
    } else if (page.title === 'RATE THIS APP!') {
      AppRate.preferences.storeAppURL = {
        ios: '1203876613',
        android: 'market://details?id=com.northpoledesign.rinkrater',
      };

      AppRate.promptForRating(true);
    } else if (page.title === 'LOG OUT') {
      this.firebaseAuth.logout();
      window.location.reload(false);
    } else {
      this.nav.setRoot(page.component, {}, { animate: true, direction: 'forward' });
    }
  }

  menuClosed() {
    console.log('menu closed');
    this.events.publish('menu:closed', '');
    this.state = 'out';
    this.staggeringPages = [];
    this.next = 0;
    console.log(this.staggeringPages);
  }

  menuOpened() {
    this.events.publish('menu:opened', '');
    this.state = 'in';
    this.itemStateDone();
  }

  showNpd() {
    this.platform.ready().then(
      () => new InAppBrowser(NORTH_POLE_URL, '_blank')
    );
  }
}
