import { Component, Inject, OnInit, Injector } from "@angular/core";
import { UtilsService } from "../../shared/utils";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Auth } from "src/app/services/auth";
import { UserService } from "src/app/services/user";
import { ToastService } from "src/app/services/toast";
import { Platform, ToastController } from "@ionic/angular";
import { Keyboard } from "@capacitor/keyboard";
import { BasePage } from "../base-page/base-page";
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { TERMS_OF_USE } from "src/app/shared/constants";

@Component({
    selector: 'app-auth',
    templateUrl: './auth.page.html',
    styleUrls: ['./auth.page.scss'],
})
export class AuthPage extends BasePage implements OnInit {

    form: FormGroup;
    authType: string;
    firebase: any;
    isAndroid: boolean;

    email: string;

    constructor(
        private fb: FormBuilder,
        private auth: Auth,
        private user: UserService,
        private utilsService: UtilsService,
        private toastService: ToastService,
        private platform: Platform,
        private toastController: ToastController,
        private injector: Injector,
        private iab: InAppBrowser
    ) {
        super(injector)

        this.isAndroid = platform.is('android');
        this.authType = this.nav.getQueryParams()?.authType;

        console.log('authType:', this.authType);

        let formGroups = {
            'login': {
                email: ['', Validators.required],
                password: ['', Validators.required]
            },
            'register': {
                fullName: ['', Validators.required],
                email: ['', Validators.required],
                password: ['', Validators.required],
                confirm: ['', Validators.required]
            },
            'reset': {
                email: ['', Validators.required]
            }
        };

        this.form = this.fb.group(formGroups[this.authType]);
        this.firebase = firebase;
    }

    ngOnInit() {
    }

    onReset() {
        const form: any = this.form.value;
        const email: string = form.email;

        this.utilsService.showLoading('Loading...', 'Authenticating');

        this.auth
            .reset(email)
            .subscribe(
                (response: any) => {
                    this.utilsService.hideLoading();
                    this.utilsService.alert('Password reset sent to your email address.');
                    this.popToRoot();
                },
                (error: any) => {
                    this.utilsService.hideLoading();
                    this.utilsService.alert(error.message, 'Reset password error');

                }
            );
    }

    doAuth(ev) {

        Keyboard.hide();

        this.utilsService.showLoading('Loading...', 'Authenticating');

        const formValue = this.form.value;

        switch (this.authType) {
            case 'login':
                this.auth
                    .login(formValue.email, formValue.password)
                    .subscribe(
                        (data) => {
                            console.log('data', data);
                            this.utilsService.hideLoading();
                        },
                        async (error) => {
                            this.utilsService.hideLoading();
                            if (this.platform.is('cordova'))
                                this.toastService.show(error.message).subscribe();
                            else {
                                let toast = await this.toastController.create({
                                    message: error.message,
                                    duration: 3000
                                });
                                toast.present();
                            }
                        }
                    );
                break;
            case 'register':
                formValue.provider = 'email';
                this.auth
                    .signUp(formValue.email, formValue.password)
                    .subscribe(
                        async (data) => {
                            this.utilsService.hideLoading();

                            let user = this.firebase.auth().currentUser;
                            user.sendEmailVerification().then(
                                () => { this.user.createUser(data.uid, formValue); },
                                console.log
                            );

                            if (this.platform.is('cordova'))
                                this.toastService.show('User created successfully! WooHoo!').subscribe();
                            else {
                                let toast = await this.toastController.create({
                                    message: 'User created successfully! WooHoo!',
                                    duration: 3000
                                });
                                toast.present();
                            }
                        },
                        async (error) => {
                            this.utilsService.hideLoading();
                            if (this.platform.is('cordova'))
                                this.toastService.show(error.message).subscribe();
                            else {
                                let toast = await this.toastController.create({
                                    message: error.message,
                                    duration: 3000
                                });
                                toast.present();
                            }
                        }
                    );
                break;
        }
    }

    isPasswordMatch() {
        const val = this.form.value;
        switch (this.authType) {
            case 'login':
                return true;
            case 'register':
                return val && val.password && val.password == val.confirm;
        }
    }

    openTOS(): void {
        this.platform.ready().then(
            () => this.iab.create(TERMS_OF_USE, '_blank')
        );
    }

    openAuth() {

        setTimeout(
            () => {
                if (this.authType == 'register')
                    this.nav.push('auth', { authType: 'login' });
                else
                    this.nav.push('auth', { authType: 'register' });
            },
            300
        );
    }

    onNavigate(authType: string = '') {
        setTimeout(
            () => {
                switch (authType) {
                    case 'login':
                        this.nav.push('auth', { authType: 'login' });
                        break;
                    case 'register':
                        this.nav.push('auth', { authType: 'register' });
                        break;
                    default:
                        this.nav.push('auth', { authType: 'reset' });

                }
            }
        );
    }

}
