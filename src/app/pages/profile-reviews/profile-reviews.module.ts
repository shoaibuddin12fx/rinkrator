import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileReviewsPageRoutingModule } from './profile-reviews-routing.module';

import { ProfileReviewsPage } from './profile-reviews.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfileReviewsPageRoutingModule
  ],
  declarations: [ProfileReviewsPage]
})
export class ProfileReviewsPageModule {}
