import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileReviewsPage } from './profile-reviews.page';

const routes: Routes = [
  {
    path: '',
    component: ProfileReviewsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileReviewsPageRoutingModule {}
