import { Component, OnInit, Injector } from "@angular/core";
import { UtilsService } from "../../shared/utils";
import { ReviewsService } from "../../services/review";
import { RinksService } from "../../services/rink";
import { Review } from "src/app/shared/review";
import { Observable } from "rxjs";
import { BasePage } from "../base-page/base-page";
import * as _ from "lodash";

@Component({
  selector: 'app-profile-reviews',
  templateUrl: './profile-reviews.page.html',
  styleUrls: ['./profile-reviews.page.scss'],
})
export class ProfileReviewsPage extends BasePage implements OnInit {

  user: any;
  reviews: Review[];
  reviewsPerRink: Array<{ key: string, reviews: Review[], toggle: boolean, iconRight: string }> = [];
  rinkIds: Array<string> = [];
  private previous: number;

  constructor(
    private reviewsService: ReviewsService,
    private rinksService: RinksService,
    private utilsService: UtilsService,
    private injector: Injector
  ) {
    super(injector)
    this.user = this.getQueryParams()?.user;
  }

  ngOnInit() {
    this.getReviews();
  }

  private getReviews(): void {
    this.reviewsService
      .getAllReviewsForUser(this.user.$key)
      .subscribe(
        (reviews) => {
          this.reviews = reviews;

          console.log(this.reviews);

          this.reviews.forEach((review) => {
            this.rinkIds.push(review.rinkId);
          });

          this.rinkIds = _.uniq(this.rinkIds);

          this.rinkIds.forEach((id, idx) => {
            this.reviewsPerRink[idx] = { key: '', reviews: [], toggle: false, iconRight: 'add' };
            this.reviewsPerRink[idx].reviews = [];
            this.reviews.forEach((review) => {
              if (review.rinkId === id) {
                this.reviewsPerRink[idx].key = id;
                this.reviewsPerRink[idx].reviews.push(review);
              }
            });
          });
        },
        (error) => this.utilsService.alert(error)
      );
  }

  getRinkByKey(key: string): Observable<any> {
    return this.rinksService.getRinkByKey(key);
  }

  goSettings() {
    setTimeout(
      () => this.nav.push('settings'),
      300
    );
  }

  goReview(review: Review) {
    setTimeout(
      () => this.nav.push('add-review', { key: review.rinkId, review: review }),
      300
    );
  }

  toggle(idx: number) {

    this.reviewsPerRink[idx].toggle = !this.reviewsPerRink[idx].toggle;
    this.reviewsPerRink[idx].iconRight = this.reviewsPerRink[idx].iconRight === 'add' ? 'remove' : 'add';

    if (!this.previous && this.previous != 0)
      this.previous = idx;
    else {
      if (this.previous != idx) {
        this.reviewsPerRink[this.previous].toggle = false;
        this.reviewsPerRink[this.previous].iconRight = 'add';
        this.previous = idx;
      }
    }
  }

}
function AddReviewPage(AddReviewPage: any, arg1: { key: string; review: Review; }): void {
  throw new Error("Function not implemented.");
}

