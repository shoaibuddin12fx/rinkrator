import { Component, OnInit, Injector } from "@angular/core";
import { EmailComposer } from "@ionic-native/email-composer/ngx";
import { Platform } from "@ionic/angular";
import { UtilsService } from "../../shared/utils";
import { BasePage } from "../base-page/base-page";
import { SettingsPage } from "../settings/settings.page";


@Component({
  selector: 'app-rink-info-details',
  templateUrl: './rink-info-details.page.html',
  styleUrls: ['./rink-info-details.page.scss'],
})
export class RinkInfoDetailsPage extends BasePage implements OnInit {

  rink: any;

  constructor(
    private platform: Platform,
    private utilsService: UtilsService,
    private injector: Injector,
    private emailComposer: EmailComposer
  ) {
    super(injector)
    this.rink = this.getQueryParams()?.rink;
  }

  private isNotEmpty(value: string): boolean {
    return (value && value.trim() != '');
  }

  call(): void {
    this.platform.ready().then(() => {
      if (this.isNotEmpty(this.rink.phone)) {
        CallNumber
          .callNumber(this.rink.phone, true)
          .then(
            () => console.log('Launched dialer!'),
            error => this.utilsService.alert(error)
          );
      } else
        this.utilsService.alert('This Rink does not provide a phone number', 'Call Rink');
    });
  }

  web(): void {
    this.platform.ready().then(() => {
      if (this.isNotEmpty(this.rink.website)) {
        new InAppBrowser(this.rink.website, '_blank');
      } else
        this.utilsService.alert('This Rink does not provide a link to a website', 'Rink Site');
    });
  }

  openEmail() {
    if (this.platform.is('cordova')) {

      let email = {
        to: 'senan@rinkrater.com',
        subject: '',
        body: '',
        isHtml: true
      };
      this.emailComposer.open(email);
    }
  }

  map(): void {

    const options = { timeout: 10000, enableHighAccuracy: true };

    this.utilsService.showLoading('Loading...', 'Map Navigator');

    Geolocation.getCurrentPosition(options)
      .then(resp => {
        const lat = resp.coords.latitude;
        const lng = resp.coords.longitude;
        const options: LaunchNavigatorOptions = {
          start: lat + ', ' + lng
        };

        this.utilsService.hideLoading();

        LaunchNavigator.navigate([this.rink.lat, this.rink.long], options)
          .then(
            success => console.log('Launched navigator'),
            error => console.log('Error launching navigator', error)
          );
      }, (err) => {
        console.log('Geolocation err: ' + JSON.stringify(err));
      });
  }

  goSettings() {
    setTimeout(
      () => this.nav.push('settings'),
      300
    );
  }

  ngOnInit() {
  }

}
