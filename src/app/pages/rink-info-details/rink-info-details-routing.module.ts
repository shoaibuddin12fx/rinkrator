import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RinkInfoDetailsPage } from './rink-info-details.page';

const routes: Routes = [
  {
    path: '',
    component: RinkInfoDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RinkInfoDetailsPageRoutingModule {}
