import { Component, ViewChild, Inject, OnInit, Injector, NgZone } from "@angular/core";
import { UtilsService } from "../../shared/utils";
import { Platform, NavController } from "@ionic/angular";
import { Observable } from "rxjs";
import { BasePage } from "../base-page/base-page";

@Component({
  selector: 'app-nearby',
  templateUrl: './nearby.page.html',
  styleUrls: ['./nearby.page.scss'],
})
export class NearbyPage extends BasePage implements OnInit {

  map;
  accordion: Array<{ category: string, leftImage: string, toggle: boolean, iconRight: string, search: string }>;
  location: GoogleMapsLatLng;
  youMarker;
  markers = [];
  places = [];
  pagination: any;
  private previous: number;
  private isLoadMore: boolean = false;

  constructor(
    private platform: Platform,
    private utilsService: UtilsService,
    private zone: NgZone,
    private injector: Injector
  ) {
    super(injector)

    this.accordion = [
      { category: 'DINING', leftImage: 'assets/img/nearby/nearby_icons_dining.png', toggle: false, iconRight: 'add', search: 'restaurant' },
      { category: 'COFFEE SHOPS', leftImage: 'assets/img/nearby/nearby_icons_coffee.png', toggle: false, iconRight: 'add', search: 'cafe' },
      { category: 'BARS', leftImage: 'assets/img/nearby/nearby_icons_bars.png', toggle: false, iconRight: 'add', search: 'bar' },
      { category: 'SHOPPING', leftImage: 'assets/img/nearby/nearby_icons_shopping.png', toggle: false, iconRight: 'add', search: 'shopping_mall' },
      { category: 'LODGING', leftImage: 'assets/img/nearby/nearby_icons_lodging.png', toggle: false, iconRight: 'add', search: 'lodging' },
      { category: 'LOCAL ATTRACTIONS', leftImage: 'assets/img/nearby/nearby_icons_attractions.png', toggle: false, iconRight: 'add', search: 'park' },
      { category: 'MEDICAL', leftImage: 'assets/img/nearby/nearby_icons_medical.png', toggle: false, iconRight: 'add', search: 'hospital' },
      { category: 'GAS STATIONS', leftImage: 'assets/img/nearby/nearby_icons_gasl.png', toggle: false, iconRight: 'add', search: 'gas_station' }
    ];
  }

  ionViewWillLeave() {
    this.deleteMarkers();
    this.youMarker.remove();
    this.places = [];
  }

  ngOnInit() {

    this.platform.ready().then(
      () => {
        this.utilsService.showLoading('Loading map...', 'Nearby Map');
        setTimeout(
          () => {
            this.map = new GoogleMap('map');

            this.map.one(GoogleMapsEvent.MAP_READY).then(
              () => {

                this.utilsService.hideLoading();
                this.getCurrentLocation().subscribe(location => {

                  console.log(location);

                  let markerOptions: GoogleMapsMarkerOptions = {
                    position: location,
                    title: 'You are here!'
                  };

                  this.map.addMarker(markerOptions).then((marker: GoogleMapsMarker) => {
                    this.youMarker = marker;
                    this.map.animateCamera({
                      target: location,
                      zoom: 15,
                      duration: 1000
                    }).then(() => {
                      marker.showInfoWindow();
                    });
                    // this.searchNearby('restaurant', 0);
                  });
                });
              }
            );
          },
          3000
        );
      }
    );
  }

  setMapOnAll(map) {
    for (let i = 0; i < this.markers.length; i++) {
      this.markers[i].remove();
    }
  }

  clearMarkers() {
    this.setMapOnAll(null);
  }

  deleteMarkers() {
    this.clearMarkers();
    this.markers = [];
    this.places = [];
  }

  createMarkers(place) {

    let lat = place.geometry.location.lat();
    let lng = place.geometry.location.lng();
    let location = new GoogleMapsLatLng(lat, lng);
    let self = this;

    let markerOptions = {
      position: location,
      title: place.name,
      markerClick: function (marker) {
        self.launchNavigator(place);
      }
    };

    this.map.addMarker(markerOptions).then(
      (marker) => {
        marker.showInfoWindow();
        this.markers.push(marker);
      },
      err => console.log(err)
    );
  }

  markerVisible(place) {
    console.log(this.markers);
    this.clearMarkers();
    this.markers = [];
    this.createMarkers(place);
  }

  launchNavigator(place) {

    console.log(place);

    let currentLocation = this.location;
    let lat = place.geometry.location.lat();
    let lng = place.geometry.location.lng();

    let options: LaunchNavigatorOptions = {
      start: currentLocation.lat + ', ' + currentLocation.lng
    };

    LaunchNavigator.navigate([lat, lng], options)
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
  }

  loadMore(hasNextPage: boolean) {
    if (hasNextPage) {
      this.isLoadMore = true;
      this.pagination.nextPage();
    }
  }

  searchNearby(type: string, idx: number) {
    let service;
    let tmpMap = new google.maps.Map(document.getElementById('tmpMap'), {
      center: this.location,
      zoom: 15
    });
    let request = {
      location: this.location,
      radius: '5000',
      type: [type]
    };

    service = new google.maps.places.PlacesService(tmpMap);
    service.nearbySearch(request, (results, status, pagination) => {

      this.pagination = pagination;

      if (!this.isLoadMore)
        this.deleteMarkers();

      if (status == google.maps.places.PlacesServiceStatus.OK) {

        console.log('status ok');
        console.log('results length', results.length);
        for (let i = 0; i < results.length; i++) {
          let place = results[i];

          if (place.types[0] == type) {
            this.places.push(place);
            this.createMarkers(results[i]);
          }
        }
        console.log('places length', this.places.length);

        if (!this.isLoadMore) {
          this.toggle(idx);
        } else
          this.isLoadMore = false;
      }
    });
  }

  goSearch() {
    setTimeout(
      () => this.nav.setRoot('search', { animate: true, direction: 'back' }),
      300
    );
  }

  goSettings() {
    setTimeout(
      () => this.nav.push('settings'),
      300
    );
  }

  getCurrentLocation() {

    let options = { timeout: 10000, enableHighAccuracy: true };

    let locationObs = Observable.create(observable => {
      Geolocation.getCurrentPosition(options)
        .then(resp => {

          console.log(resp);

          let lat = resp.coords.latitude;
          let lng = resp.coords.longitude;

          this.location = new GoogleMapsLatLng(lat, lng);

          // Farmington
          // 41.727937, -72.826382

          // Test location
          // this.location = new GoogleMapsLatLng(41.727937, -72.826382);

          observable.next(this.location);
        }, (err) => {
          console.log('Geolocation err: ' + JSON.stringify(err));
        });
    });

    return locationObs;
  }

  private toggle(idx: number) {

    this.accordion[idx].toggle = !this.accordion[idx].toggle;
    this.accordion[idx].iconRight = this.accordion[idx].iconRight === 'add' ? 'remove' : 'add';

    if (!this.previous && this.previous != 0)
      this.previous = idx;
    else {
      if (this.previous != idx) {
        this.accordion[this.previous].toggle = false;
        this.accordion[this.previous].iconRight = 'add';
        this.previous = idx;
      }
    }
  }

}
