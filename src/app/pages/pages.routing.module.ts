import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'about',
    pathMatch: 'full'
  },
  {
    path: 'about',
    loadChildren: () => import('./about/about.page').then(m => m.AboutPage)
  },
  {
    path: 'add-rink',
    loadChildren: () => import('./add-rink/add-rink.module').then(m => m.AddRinkPageModule)
  },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthPageModule)
  },
  {
    path: 'contact',
    loadChildren: () => import('./contact/contact.module').then(m => m.ContactPageModule)
  },
  {
    path: 'main',
    loadChildren: () => import('./main/main.module').then(m => m.MainPageModule)
  },
  {
    path: 'nearby',
    loadChildren: () => import('./nearby/nearby.module').then(m => m.NearbyPageModule)
  },
  {
    path: 'partners',
    loadChildren: () => import('./partners/partners.module').then(m => m.PartnersPageModule)
  },
  {
    path: 'profile-card',
    loadChildren: () => import('./profile-card/profile-card.module').then(m => m.ProfileCardPageModule)
  },
  {
    path: 'profile-edit',
    loadChildren: () => import('./profile-edit/profile-edit.module').then(m => m.ProfileEditPageModule)
  },
  {
    path: 'profile-favorites',
    loadChildren: () => import('./profile-favorites/profile-favorites.module').then(m => m.ProfileFavoritesPageModule)
  },
  {
    path: 'profile-photos',
    loadChildren: () => import('./profile-photos/profile-photos.module').then(m => m.ProfilePhotosPageModule)
  },
  {
    path: 'profile-reviews',
    loadChildren: () => import('./profile-reviews/profile-reviews.module').then(m => m.ProfileReviewsPageModule)
  },
  {
    path: 'rink-info',
    loadChildren: () => import('./rink-info/rink-info.module').then(m => m.RinkInfoPageModule)
  },
  {
    path: 'rink-info-details',
    loadChildren: () => import('./rink-info-details/rink-info-details.module').then(m => m.RinkInfoDetailsPageModule)
  },
  {
    path: 'search',
    loadChildren: () => import('./search/search.module').then(m => m.SearchPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./settings/settings.module').then(m => m.SettingsPageModule)
  },
  {
    path: 'add-review',
    loadChildren: () => import('./add-review/add-review.module').then(m => m.AddReviewPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }