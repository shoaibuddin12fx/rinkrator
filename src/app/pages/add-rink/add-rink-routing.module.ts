import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddRinkPage } from './add-rink.page';

const routes: Routes = [
  {
    path: '',
    component: AddRinkPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddRinkPageRoutingModule {}
