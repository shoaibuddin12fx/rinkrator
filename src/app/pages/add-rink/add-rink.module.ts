import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddRinkPageRoutingModule } from './add-rink-routing.module';

import { AddRinkPage } from './add-rink.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddRinkPageRoutingModule
  ],
  declarations: [AddRinkPage]
})
export class AddRinkPageModule {}
