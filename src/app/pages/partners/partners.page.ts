import { Component, OnInit, Injector } from "@angular/core";
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { Platform } from "@ionic/angular";
import { BasePage } from "../base-page/base-page";

@Component({
  selector: 'app-partners',
  templateUrl: './partners.page.html',
  styleUrls: ['./partners.page.scss'],
})
export class PartnersPage extends BasePage implements OnInit {

  private previous: number;
  private isCordova: boolean;
  accordion: Array<{ category: string, query: string, toggle: boolean, iconRight: string }>;

  constructor(private platform: Platform, private injector: Injector, private emailComposer: EmailComposer,) {
    super(injector)

    this.accordion = [
      { category: 'ORGANIZATIONS', query: 'organizations', toggle: false, iconRight: 'add' },
      { category: 'HOCKEY COMPANIES', query: 'hockey_companies', toggle: false, iconRight: 'add' },
      { category: 'HOCKEY PRODUCTS', query: 'hockey_products', toggle: false, iconRight: 'add' },
      { category: 'CAMPS / COACHES / CLINICS', query: 'camps_coaches_clinics', toggle: false, iconRight: 'add' },
      { category: 'HOCKEY SOCIAL MEDIA', query: 'hockey_social_media', toggle: false, iconRight: 'add' }
    ];
    this.isCordova = platform.is('cordova') || false;
  }

  ngOnInit() {
  }

  onNavigateSettings() {
    setTimeout(() => {
      this.nav.push('settings');
    }, 300);
  }

  onToggle(idx: number) {
    this.accordion[idx].toggle = !this.accordion[idx].toggle;
    this.accordion[idx].iconRight = this.accordion[idx].iconRight === 'add' ? 'remove' : 'add';

    if (!this.previous && this.previous != 0)
      this.previous = idx;
    else {
      if (this.previous != idx) {
        this.accordion[this.previous].toggle = false;
        this.accordion[this.previous].iconRight = 'add';
        this.previous = idx;
      }
    }
  }

  goSearch() {
    setTimeout(
      () => this.nav.setRoot('search', { animate: true, direction: 'back' }),
      300
    );
  }

  onAccessEmail(): void {
    this.platform.ready()
      .then(() => {
        if (this.isCordova) {
          let email: any = {
            to: 'senan@rinkrater.com',
            subject: '',
            body: '',
            isHtml: true
          };
          this.emailComposer.open(email);
        }
      });
  }
}