import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/compat/database';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage extends BasePage implements OnInit {
  private previous: number = 0;
  accordion: Array<{ header: string, toggle: boolean, iconRight: string }>;
  about: AngularFireObject<any>;

  constructor(private angularFireDatabase: AngularFireDatabase, injector: Injector) {
    super(injector);
    this.accordion = [
      { header: 'What is Rink Rater?', toggle: true, iconRight: 'remove' },
      { header: 'Who Are We?', toggle: false, iconRight: 'add' },
      { header: 'Rink Rater Gear Store?', toggle: false, iconRight: 'add' }
    ];
  }

  ionViewDidLoad() {
    this.about = this.angularFireDatabase.object(`about`);
  }

  ngOnInit() {
  }

}
