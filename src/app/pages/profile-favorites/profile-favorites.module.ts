import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileFavoritesPageRoutingModule } from './profile-favorites-routing.module';

import { ProfileFavoritesPage } from './profile-favorites.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfileFavoritesPageRoutingModule
  ],
  declarations: [ProfileFavoritesPage]
})
export class ProfileFavoritesPageModule {}
