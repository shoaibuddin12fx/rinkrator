import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileFavoritesPage } from './profile-favorites.page';

const routes: Routes = [
  {
    path: '',
    component: ProfileFavoritesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileFavoritesPageRoutingModule {}
