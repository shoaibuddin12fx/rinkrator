import { Component, ViewChild, Inject, OnInit, Injector } from "@angular/core";
import { UtilsService } from "../../shared/utils";
import { Auth } from "src/app/services/auth";
import { UserService } from "src/app/services/user";
import { ToastService } from "src/app/services/toast";
import { AuthPage } from "../auth/auth.page";
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { Observable } from "rxjs";
import { PRIVACY_POLICY, TERMS_OF_USE } from "src/app/shared/constants";
import { Platform } from "@ionic/angular";
import { AngularFireDatabase } from "@angular/fire/compat/database";
import { BasePage } from "../base-page/base-page";
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage extends BasePage implements OnInit {

  slideOptions: any = {
    loop: true,
    pager: true,
    autoplay: 2000
  };
  ref: any;

  constructor(
    private toastService: ToastService,
    private auth: Auth,
    private platform: Platform,
    private userService: UserService,
    private utilsService: UtilsService,
    private emailComposer: EmailComposer,
    private camera: Camera,
    private af: AngularFireDatabase,
    private injector: Injector,
    private iab: InAppBrowser
  ) {
    super(injector)
    this.ref = af.database.ref();
  }

  ngOnInit() {
  }

  authRedirect(type: string): void {
    setTimeout(
      () => this.nav.push('auth', { authType: type }),
      300
    );
  }

  facebookLogin(): void {
    // show native spinner
    this.utilsService.showLoading('Loading...', 'Authenticating Facebook');

    let authRes: any;

    Observable.fromPromise(this.platform.ready())
      .flatMap(
        () => this.auth.facebookNativeLogin()
      )
      .flatMap(
        (response: any) => {
          const accessToken = response.authResponse.accessToken;
          return this.auth.facebookFirebaseLogin(accessToken);
        }
      )
      .flatMap(
        (response: any) => {
          authRes = response;
          const promise = this.ref.child(`/users/${authRes.uid}`).once('value');
          return Observable.fromPromise(promise);
        }
      )
      .subscribe(
        (snapshot: any) => {
          const user = {
            email: authRes.email || '',
            alias: 'Hockey Fan',
            fullName: authRes.displayName,
            photo: {
              src: authRes.photoURL || 'assets/img/common/silhoutte.png',
              raw: null
            },
            coverPhoto: {
              src: 'assets/img/profile/myprofile_bkgrnd_photo.png',
              raw: null
            },
            provider: 'facebook'
          };
          if (!snapshot.val())
            this.userService.createUser(authRes.uid, user);

          // show success toast to user
          this.toastService.show('Facebook logged in successfully!').subscribe();
        },
        (error: any) => {
          this.displayErrorHideSpinner(error);
        },
        () => this.utilsService.hideLoading()
      );
  }

  twitterLogin(): void {
    // show native spinner
    this.utilsService.showLoading('Loading...', 'Authenticating Twitter');
    let authRes: any;

    Observable
      .fromPromise(this.platform.ready())
      .flatMap(
        () => this.auth.twitterNativeLogin()
      )
      .flatMap(
        (response: any) => {
          const token = response.token;
          const secret = response.secret;

          console.log('token: ', token);
          console.log('secret: ', secret);

          return this.auth.twitterFirebaseLogin(token, secret);
        }
      )
      .flatMap(
        (response: any) => {
          authRes = response;

          console.log('authRes: ', authRes);

          const promise = this.ref.child(`/users/${authRes.uid}`).once('value');
          return Observable.fromPromise(promise);
        }
      )
      .subscribe(
        (snapshot: any) => {

          const user = {
            email: authRes.email || '',
            alias: 'Hockey Fan',
            fullName: authRes.displayName,
            photo: authRes.photoURL,
            coverPhoto: 'assets/img/profile/myprofile_bkgrnd_photo.png',
            provider: 'twitter'
          };
          if (!snapshot.val())
            this.userService.createUser(authRes.uid, user);
          this.toastService.show('Twitter logged in successfully!').subscribe();
        },
        (error: any) => {
          console.log('error: ', error);
          this.displayErrorHideSpinner(error);
        },
        () => this.utilsService.hideLoading()
      );
  }

  openInAppBrowser(type): void {
    const isPrivacy: boolean = (type == 'privacy');
    const link: string = isPrivacy ? PRIVACY_POLICY : TERMS_OF_USE;
    this.platform.ready().then(
      () => this.iab.create(link, '_blank')
    );
  }

  private displayErrorHideSpinner(error: any): void {
    const message = error.message || 'Something went wrong';
    this.toastService.show(message).subscribe();
    this.utilsService.hideLoading();
  }

}
