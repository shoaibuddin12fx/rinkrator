import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilePhotosPageRoutingModule } from './profile-photos-routing.module';

import { ProfilePhotosPage } from './profile-photos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfilePhotosPageRoutingModule
  ],
  declarations: [ProfilePhotosPage]
})
export class ProfilePhotosPageModule {}
