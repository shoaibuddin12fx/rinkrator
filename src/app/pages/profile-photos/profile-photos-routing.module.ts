import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilePhotosPage } from './profile-photos.page';

const routes: Routes = [
  {
    path: '',
    component: ProfilePhotosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilePhotosPageRoutingModule {}
