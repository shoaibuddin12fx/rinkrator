import { Component, Inject, Injector, OnInit, } from "@angular/core";
import { UtilsService } from "../../shared/utils";
import { UserService } from "src/app/services/user";
import { BasePage } from "../base-page/base-page";
import { FirebaseApp } from "@angular/fire/compat";

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.page.html',
  styleUrls: ['./profile-edit.page.scss'],
})
export class ProfileEditPage extends BasePage implements OnInit {

  user: any;
  currentUser: any;

  constructor(
    private userService: UserService,
    private utilsService: UtilsService,
    private firebaseAuth: FirebaseAuth,
    private injector: Injector,
    @Inject(FirebaseApp) firebaseApp: any
  ) {
    super(injector)
    this.user = this.getQueryParams()?.user;
    this.currentUser = firebaseApp.auth().currentUser;
  }

  ngOnInit() {
  }

  private saveUser(form): void {

    delete form.password;
    delete form.confirmPassword;

    this.userService
      .saveUser(this.user.$key, form)
      .subscribe(
        () => {
          this.utilsService.alert('User updated successfully!', 'Update Info');
          this.nav.pop();
        },
        error => this.utilsService.alert(`Error: ${error}`, 'Rink Error')
      );
  }

  submit(form) {

    console.log(form);

    if (!!form.confirmPassword && !!form.password) {
      console.log('has confirm and password');
      if (form.confirmPassword == form.password) {
        console.log('is equal');
        this.currentUser
          .updatePassword(form.password)
          .then(
            () => {
              console.log('password updated!');
              this.saveUser(form);
            },
            (error) => {
              this.utilsService.alert(error.message, 'Password Update failed');
              this.firebaseAuth.logout();
            }
          );
      } else
        this.utilsService.alert('Confirm Password should match with the password.', 'Verification failed');
    } else
      this.saveUser(form);
  }

  goSettings() {
    setTimeout(
      () => {
        this.nav.push('settings');
      },
      300
    );
  }

}
