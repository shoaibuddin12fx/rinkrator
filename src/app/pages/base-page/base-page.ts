import { NavService } from "src/app/services/nav.service";
import {Injector} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';


export abstract class BasePage {
    public nav: NavService;
    public router: Router;
    public activatedRoute: ActivatedRoute;

    constructor(injector: Injector) {
        // this.platform = injector.get(Platform);
        // this.users = injector.get(UserService);
        // this.sqlite = injector.get(SqliteService);
        // this.network = injector.get(NetworkService);
        // this.utility = injector.get(UtilityService);
        // this.location = injector.get(Location);
        // this.common = injector.get(CommonModule);
        // this.events = injector.get(EventsService);
        this.nav = injector.get(NavService);
        // this.formBuilder = injector.get(FormBuilder);
        // this.popover = injector.get(PopoversService);
        // this.modals = injector.get(ModalService);
        // this.menuCtrl = injector.get(MenuController);
        this.router = injector.get(Router);
        this.activatedRoute = injector.get(ActivatedRoute);       
    }

    getParams() {
        return this.activatedRoute.snapshot.params;
    }

    getQueryParams() {
        return this.activatedRoute.snapshot.queryParams;
    }

    popToRoot(){
        this.nav.navigateTo("pages/home");
    }

}