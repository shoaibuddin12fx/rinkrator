import { Component, OnInit, Injector } from "@angular/core";

import { BasePage } from "../base-page/base-page";
import { EmailComposer } from "@ionic-native/email-composer/ngx";
import { FAQS } from "src/app/shared/constants";
import { Platform } from "@ionic/angular";
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage extends BasePage implements OnInit {

  ngOnInit() {
  }

  constructor(
    private platform: Platform, injector: Injector, private emailComposer: EmailComposer, private iab: InAppBrowser) {
    super(injector)
  }

  goSettings() {
    setTimeout(
      () => this.nav.push('settings'),
      300
    );
  }

  openEmail(subject: string) {
    if (this.platform.is('cordova')) {

      let email = {
        to: 'senan@rinkrater.com',
        subject: subject,
        body: '',
        isHtml: true
      };
      this.emailComposer.open(email);
    }
  }

  goSearch() {
    setTimeout(
      () => this.nav.setRoot('search', { animate: true, direction: 'back' }),
      300
    );
  }

  openPage() {
    this.platform.ready().then(
      () => this.iab.create(FAQS, '_blank')
    );
  }

}
