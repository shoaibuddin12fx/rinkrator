import { Component, ViewChild, Inject, OnInit, Injector } from "@angular/core";
import { UtilsService } from "../../shared/utils";
import { UserService } from "src/app/services/user";
import { BasePage } from "../base-page/base-page";

@Component({
  selector: 'app-profile-card',
  templateUrl: './profile-card.page.html',
  styleUrls: ['./profile-card.page.scss'],
})
export class ProfileCardPage extends BasePage implements OnInit {

  user: any;

  constructor(
    private userService: UserService,
    private utilsService: UtilsService,
    private injector: Injector
  ) {
    super(injector)
    this.user = this.nav.getQueryParams()?.user;
  }

  ngOnInit() {
  }

  submit(form: any) {
    console.log(form);

    this.user.parentCard = form;

    this.userService
      .saveUser(this.user.$key, this.user)
      .subscribe(
        () => {
          this.utilsService.alert('User updated successfully!', 'Update Info');
        },
        error => this.utilsService.alert(`Error: ${error}`, 'Rink Error')
      );
  }

  goSettings() {
    setTimeout(
      () => this.nav.push('settings'),
      300
    );
  }

}
