import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileCardPageRoutingModule } from './profile-card-routing.module';

import { ProfileCardPage } from './profile-card.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfileCardPageRoutingModule
  ],
  declarations: [ProfileCardPage]
})
export class ProfileCardPageModule {}
