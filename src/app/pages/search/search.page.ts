import { Component, ViewChild, Inject, OnInit, Injector } from "@angular/core";
import { Rink } from "../../shared/rink";
import { UtilsService } from "../../shared/utils";
import { SettingsPage } from "../settings/settings.page";
import { Keyboard } from "@capacitor/keyboard";
import { PartnersPage } from "../partners/partners.page";
import { GEAR_STORE, HOCKEY_RANKINGS_URL } from "src/app/shared/constants";
import { AddRinkPage } from "../add-rink/add-rink.page";
import { NearbyPage } from "../nearby/nearby.page";
import { Content } from "@angular/compiler/src/render3/r3_ast";
import { BasePage } from "../base-page/base-page";
import { Platform } from "@ionic/angular";
import { EmailComposer } from "@ionic-native/email-composer/ngx";

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
  animations: [
    trigger('buttonState', [
      state('in', style({
        transform: 'translateX(0)'
      })),
      transition('void => *', [
        style({
          transform: 'translateX(-100%)',
        }),
        animate('300ms cubic-bezier(0.175, 0.885, 0.32, 1.275)')
      ])
    ])
  ]
})
export class SearchPage extends BasePage implements OnInit {

  sdkDb: any;

  @ViewChild('menu') menu;
  @ViewChild(Content) content: Content;

  slideOptions: any = {
    loop: true,
    pager: true,
    autoplay: 2000
  };

  input: string = '';

  gearStore: string;
  hockeyRankingsUrl: string;

  rinks: Rink[];
  filtered: Rink[];
  shouldLoad: boolean = false;
  isValidSearch: boolean = false;
  isCordova: boolean;
  shouldShowCancel: boolean = false;
  totalSearch: number = 0;

  private isLoadMore: boolean = false;

  constructor(
    private platform: Platform,
    private utilsService: UtilsService,
    public events: Events,
    private injector: Injector,
    private emailComposer: EmailComposer,
    @Inject(FirebaseRef) fb) {

    super(injector)
    this.gearStore = GEAR_STORE;
    this.hockeyRankingsUrl = HOCKEY_RANKINGS_URL;

    this.sdkDb = fb.database().ref();
    this.isCordova = this.platform.is('cordova');
  }
  ngOnInit() {
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter...');

    setTimeout(() => this.update(), 2000);

    this.events.subscribe('menu:opened', () => {
      // your action here
      this.menu.nativeElement.style.transform = 'rotate(90deg)';
    });

    this.events.subscribe('menu:closed', () => {
      // your action here
      this.menu.nativeElement.style.transform = 'rotate(0deg)';
    });
  }

  update() {
    this.content.resize();
  }

  ionViewCanLeave() {
    console.log('Did leave');
    this.onClear();
    this.totalSearch = 0;
    this.isLoadMore = false;
    this.shouldLoad = false;
  }

  private delayMove(component: any): void {
    setTimeout(() => this.nav.setRoot(component, { animate: true, direction: 'forward' }), 500);
  }

  private showResults(snap, self) {

    let dat = snap.val();

    if (dat === null) {
      return;
    }

    this.shouldLoad = false;
    console.log('filtered', this.filtered);
    console.log('shouldLoad', this.shouldLoad);

    this.totalSearch = dat.total;

    snap.ref.off('value', self.showResults);
    snap.ref.remove();

    if (dat.hits) {
      console.log('dat.hits', dat.hits);
      if (!this.filtered) {
        this.filtered = dat.hits.map((rink) => {
          rink._source.$key = rink._id;
          return rink._source;
        });

        this.filtered = this.filtered.filter(value => {
          return value.active === 1;
        });
        this.isLoadMore = false;

        console.log('filtered done', this.filtered);
      } else {
        dat.hits.forEach((rink) => {
          rink._source.$key = rink._id;
          this.filtered.push(rink._source);
        });
        this.filtered = this.filtered.filter(value => {
          return value.active === 1;
        });
        this.filtered = _.uniqBy(this.filtered, (e) => {
          return e.$key;
        });
        this.isLoadMore = false;

        console.log('filtered done', this.filtered);
      }
    } else {
      this.totalSearch = 0;
    }

    // this.shouldLoad = false;
    // console.log('shouldLoad', this.shouldLoad);
  }

  onInAppBrowser(url: string) {
    setTimeout(() => {
      this.platform.ready()
        .then(() => {
          if (this.isCordova)
            new InAppBrowser(url, '_blank');
        });
    }, 300);
  }

  onClear(): void {
    console.log('onClear');
    this.input = '';
    this.filtered = undefined;
    this.isValidSearch = false;
  }

  errorHandler(err): void {
    this.utilsService.hideLoading();
    this.utilsService.alert(err);
  }

  go(page: string): void {
    switch (page) {
      case 'partners':
        this.delayMove(PartnersPage);
        break;
      case 'addRink':
        this.delayMove(AddRinkPage);
        break;
      case 'nearby':
        this.delayMove(NearbyPage);
        break;
      case 'settings':
        this.delayMove(SettingsPage);
        break;
    }
  }

  goInfo(rink: Rink[]): void {
    console.log('goInfo');
    this.filtered = undefined;
    this.input = '';
    Keyboard.hide();

    setTimeout(() => this.nav.setRoot('rink-info', { rink: rink }), 500);
  }

  loadMore() {
    console.log('do infinite');

    Keyboard.hide();

    if (this.isLoadMore) {
      return;
    }

    this.isLoadMore = true;

    let query = {
      index: 'firebase',
      type: 'rinks',
      q: `*${this.input}*`,
      from: this.filtered.length + 1,
      size: 15,
      body: {
        query: {
          match: {
            "active": 1,
          },
        },
      },
    };

    let ref = this.sdkDb.child('search');
    let key = ref.child('request').push(query).key;

    ref
      .child('response/' + key)
      .on('value', (snap) => this.showResults(snap, this));
  }

  onAddRink() {
    const email = {
      to: 'addrink@rinkrater.com',
      subject: 'Add a new Rink',
      body: 'Please utilize the guidelines below to provide information about a Rink / Arena you think we’re missing:<br><br>',
      isHtml: true,
    };

    email.body += 'Rink / Arena Name:<br>';
    email.body += 'Address:<br>';
    email.body += 'City:<br>';
    email.body += 'State / Province:<br>';
    email.body += 'Zip Code / Postal Code:<br>';
    email.body += 'Country:<br>';
    email.body += 'Phone Number:<br>';
    email.body += 'Website:<br><br>';
    email.body += 'Thanks very much! If it’s a rink we’ve missed, we’ll add it to our database. Please check back in 3-5 business days.<br><br>';
    email.body += 'RinkRater.com';

    this.emailComposer.open(email);
  }

  onInput(): void {

    this.shouldLoad = true;
    this.filtered = undefined;
    this.totalSearch = 0;
    this.isLoadMore = false;

    if (this.input && this.input.trim() != '' && this.input.length >= 3) {

      this.isValidSearch = true;

      let query = {
        index: 'firebase',
        type: 'rinks',
        q: `*${this.input}*`,
        from: 0,
        size: 15,
        body: {
          query: {
            match: {
              "active": 1,
            },
          },
        },
      };

      let ref = this.sdkDb.child('search');
      let key = ref.child('request').push(query).key;

      ref
        .child('response/' + key)
        .on('value', (snap) => this.showResults(snap, this));

    } else if (this.input.trim() == '') {
      this.shouldLoad = false;
    } else {
      this.filtered = undefined;
      this.isValidSearch = false;
    }
  }

  hide(ev: any): void {
    // this.onClear();
    Keyboard.hide();
  }
}

function trigger(arg0: string, arg1: any[]): any {

}

function state(arg0: string, arg1: any): any {

}

function style(arg0: { transform: string; }): any {

}

function transition(arg0: string, arg1: any[]): any {

}

function animate(arg0: string): any {

}

function FirebaseRef(FirebaseRef: any) {

}

