import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RinkInfoPage } from './rink-info.page';

const routes: Routes = [
  {
    path: '',
    component: RinkInfoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RinkInfoPageRoutingModule {}
