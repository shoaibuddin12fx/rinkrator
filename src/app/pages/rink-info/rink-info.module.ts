import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RinkInfoPageRoutingModule } from './rink-info-routing.module';

import { RinkInfoPage } from './rink-info.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RinkInfoPageRoutingModule
  ],
  declarations: [RinkInfoPage]
})
export class RinkInfoPageModule {}
