import { Component, ViewChild, OnInit, Injector } from "@angular/core";
import { UtilsService } from "../../shared/utils";
import { ReviewsService } from "../../services/review";
import { RinksService } from "../../services/rink";
import { RatingsService } from "../../services/rating";
import { SettingsPage } from "../settings/settings.page";
import { SearchPage } from "../search/search.page";
import { BASE_URL } from "src/app/shared/constants";
import { RinkInfoDetailsPage } from "../rink-info-details/rink-info-details.page";
import { Content } from "@angular/compiler/src/render3/r3_ast";
import { Platform } from "@ionic/angular";
import { AngularFireDatabase } from "@angular/fire/compat/database";
import { BasePage } from "../base-page/base-page";

@Component({
  selector: 'app-rink-info',
  templateUrl: './rink-info.page.html',
  styleUrls: ['./rink-info.page.scss'],
})
export class RinkInfoPage extends BasePage implements OnInit {

  uid: string;
  location: any;
  isFavorite: string | number | boolean = false;
  rink: any;
  overall: number = 0;
  reviewCounts: number = 0;
  ratings: any;
  hasToolBar: boolean = true;
  isCordova: boolean;

  @ViewChild(Content) content: Content;
  @ViewChild('reviewCategories') reviewCategories;
  @ViewChild('star1') s1;
  @ViewChild('star2') s2;
  @ViewChild('star3') s3;
  @ViewChild('star4') s4;
  @ViewChild('star5') s5;

  constructor(
    private platform: Platform,
    private utilsService: UtilsService,
    private reviewsService: ReviewsService,
    private ratingsService: RatingsService,
    private rinksService: RinksService,
    private firebaseAuth: FirebaseAuth,
    private angularFireDatabase: AngularFireDatabase,
    private injector: Injector
  ) {
    super(injector)
    this.isCordova = platform.is('cordova');
    this.rink = this..get('rink');
    console.log(this.rink);
    this.firebaseAuth
      .subscribe(
        auth => {
          if (!!auth)
            this.uid = auth.uid;

          this.angularFireDatabase
            .object(`user-rinksfav/${this.uid}/${this.rink.$key}`) //rinksFavPerUser
            .subscribe(
              (data: any) => this.isFavorite = data.$value,
              (error: any) => this.utilsService.alert(error)
            );
        },
        (error: any) => this.utilsService.alert(error)
      );
  }

  ionViewWillEnter() {
    console.log(this.reviewCategories);
    const rect = this.reviewCategories.nativeElement.getBoundingClientRect();
    this.content.addScrollListener((event) => {

      const scrollTop = event.target.scrollTop;

      if (scrollTop >= rect.top)
        this.onToolbarChange(false);
      else
        this.onToolbarChange(true);
    });
  }

  private onToolbarChange(status: boolean): void {
    this.hasToolBar = status;
    this.content.resize();
  }

  private getRatings(): void {
    this.ratingsService
      .getAllRatingsForRink(this.rink.$key)
      .subscribe(
        (ratings: any) => {
          this.ratings = ratings;
          this.ratings.forEach((review) => {
            this.overall += review.rating;
          });
          this.overall = Math.round(this.overall / this.ratings.length);
          this.onRating();
        },
        (error: any) => this.utilsService.alert(error)
      );
  }

  private getReviewCounts(): void {
    this.reviewsService
      .getAllReviewsForRink(this.rink.$key)
      .subscribe(
        (reviews: any) => {
          this.reviewCounts = reviews.length;
        }
      )
  }

  private isNotEmpty(value: string): boolean {
    return (value && value.trim() != '');
  }

  private onRating(): void {
    if (this.overall == 1) {
      this.s1.nativeElement.style.opacity = 1;
      this.s2.nativeElement.style.opacity = .5;
      this.s3.nativeElement.style.opacity = .5;
      this.s4.nativeElement.style.opacity = .5;
      this.s5.nativeElement.style.opacity = .5;
    } else if (this.overall == 2) {
      this.s1.nativeElement.style.opacity = 1;
      this.s2.nativeElement.style.opacity = 1;
      this.s3.nativeElement.style.opacity = .5;
      this.s4.nativeElement.style.opacity = .5;
      this.s5.nativeElement.style.opacity = .5;
    } else if (this.overall == 3) {
      this.s1.nativeElement.style.opacity = 1;
      this.s2.nativeElement.style.opacity = 1;
      this.s3.nativeElement.style.opacity = 1;
      this.s4.nativeElement.style.opacity = .5;
      this.s5.nativeElement.style.opacity = .5;
    } else if (this.overall == 4) {
      this.s1.nativeElement.style.opacity = 1;
      this.s2.nativeElement.style.opacity = 1;
      this.s3.nativeElement.style.opacity = 1;
      this.s4.nativeElement.style.opacity = 1;
      this.s5.nativeElement.style.opacity = .5;
    } else if (this.overall == 5) {
      this.s1.nativeElement.style.opacity = 1;
      this.s2.nativeElement.style.opacity = 1;
      this.s3.nativeElement.style.opacity = 1;
      this.s4.nativeElement.style.opacity = 1;
      this.s5.nativeElement.style.opacity = 1;
    }

  }

  ionViewWillLeave(): void {
    this.overall = 0;
    this.content
      .scrollToTop(500)
      .then(() => {
        this.onToolbarChange(true);
      });
  }

  ngOnInit(): void {
    this.getRatings();
    this.getReviewCounts();
  }

  setRinkAsFavorite() {
    if (this.isFavorite) {
      this.angularFireDatabase.object(`user-rinksfav/${this.uid}/${this.rink.$key}`).remove(); // rinksFavPerUser
      this.isFavorite = false;
      this.utilsService.alert('You have removed this Rink to your list of favorites', 'Favorite Rink');
    } else {
      this.rinksService
        .setRinkAsFavorite(this.uid, this.rink)
        .subscribe(
          () => this.utilsService.alert('You have added this Rink to your list of favorites', 'Favorite Rink'),
          (error: any) => this.utilsService.alert(error)
        );
    }
  }

  call(): void {

    if (this.isNotEmpty(this.rink.phone)) {
      CallNumber
        .callNumber(this.rink.phone, true)
        .then(
          () => console.log('Launched dialer!'),
          (error: any) => this.utilsService.alert(error)
        );
    } else
      this.utilsService.alert('This Rink does not provide a phone number', 'Call Rink');
  }

  web(): void {
    this.platform.ready().then(
      () => {
        if (this.isNotEmpty(this.rink.website))
          new InAppBrowser(this.rink.website, '_blank');
        else
          this.utilsService.alert('This Rink does not provide a link to a website', 'Rink Website');
      }
    );
  }

  map(): void {

    const options = { timeout: 10000, enableHighAccuracy: true };

    this.utilsService.showLoading('Loading...', 'Map Navigator');

    Geolocation.getCurrentPosition(options)
      .then(resp => {
        const lat = resp.coords.latitude;
        const lng = resp.coords.longitude;
        const options: LaunchNavigatorOptions = {
          start: lat + ', ' + lng
        };

        this.utilsService.hideLoading();

        LaunchNavigator.navigate([this.rink.lat, this.rink.long], options)
          .then(
            success => console.log('Launched navigator'),
            error => console.log('Error launching navigator', error)
          );
      }, (err) => {
        console.log('Geolocation err: ' + JSON.stringify(err));
      });
  }

  goReview(): void {
    setTimeout(
      () => this.nav.push('add-review', { rink: this.rink }),
      300
    );
  }

  goSearch() {
    setTimeout(
      () => this.nav.setRoot('search', { animate: true, direction: 'back' }),
      300
    );
  }

  goSettings(): void {
    setTimeout(
      () => this.nav.push('settings'),
      300
    );
  }

  goInfoDetails(): void {
    this.nav.push('rink-info-details', { rink: this.rink });
  }

  onInAppBrowser(key: string) {
    setTimeout(() => {
      this.platform.ready()
        .then(() => {
          if (this.isCordova)
            new InAppBrowser(`${BASE_URL}/${key}`, '_blank');
        });
    }, 300);
  }
}
function AddReviewPage(AddReviewPage: any, arg1: { rink: any; }): void {
  throw new Error("Function not implemented.");
}

