import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { PagesRoutingModule } from './pages.routing.module';

@NgModule({
  declarations: [],
  entryComponents: [],
  imports: [CommonModule, IonicModule, PagesRoutingModule],
  //   providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  providers: [

  ]
})
export class PagesModule { }