import { Component, Inject, OnInit, Injector } from "@angular/core";
import { Rink } from "../../shared/rink";
import { UtilsService } from "../../shared/utils";
import { UserService } from "src/app/services/user";
import { SearchPage } from "../search/search.page";
import { ProfileCardPage } from "../profile-card/profile-card.page";
import { ProfileEditPage } from "../profile-edit/profile-edit.page";
import { ProfileFavoritesPage } from "../profile-favorites/profile-favorites.page";
import { ProfilePhotosPage } from "../profile-photos/profile-photos.page";
import { ProfileReviewsPage } from "../profile-reviews/profile-reviews.page";
import { Platform } from "@ionic/angular";
import { FirebaseApp } from "@angular/fire/compat";
import { BasePage } from "../base-page/base-page";
import { Camera } from "@ionic-native/camera/ngx";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage extends BasePage implements OnInit {

  user: any; // TODO
  storage: any;

  constructor(
    private firebaseAuth: FirebaseAuth,
    private userService: UserService,
    private utilsService: UtilsService,
    private platform: Platform,
    private injector: Injector,
    private camera: Camera,
    @Inject(FirebaseApp) firebaseApp: any
  ) {
    super(injector)
    this.storage = firebaseApp.storage();
  }

  ngOnInit() {
  }

  ionViewDidLoad() {
    this.utilsService.showLoading('Loading...', 'User information');
    this.firebaseAuth
      .flatMap(
        (auth: any) => { return this.userService.getUser(auth.uid) }
      ).subscribe(
        user => {
          this.utilsService.hideLoading();
          this.user = user;
        },
        err => this.errorHandler.bind(this)
      );
  }

  private errorHandler(error: any): void {
    this.utilsService.hideLoading();
    this.utilsService.alert(error.message);
  }

  private getPicture(sourceType: string, photoType: string) {

    // camera options
    let opts = {
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType[sourceType],
      cameraDirection: this.camera.Direction.FRONT,
      targetHeight: 640,
      correctOrientation: true
    };

    this.utilsService.showLoading('Loading image path...', 'Get Photo');

    // get picture from camera
    this.camera.getPicture(opts).then(
      (imagePath: any) => { return this.makeFileIntoBlob(imagePath) }
      // (error) => this.onError(error)
    ).then(
      (imageBlob: any) => { return this.uploadToFirebase(imageBlob, photoType) }
      // (error) => this.onError(error)
    ).then(
      (data: any) => {
        console.log('data snapshot', data);
        this.utilsService.hideLoading();
        this.user[photoType].src = data.snapshot.downloadURL;
        this.user[photoType].raw = data.fileName;
        this.userService.saveUser(this.user.$key, this.user);
      }
      // (error) => this.onError(error)
    ).catch(
      (error) => this.onError(error)
    );
  }

  private makeFileIntoBlob(imagePath) {

    console.log('imagePath', imagePath);

    if (this.platform.is('android')) {
      return new Promise(
        (resolve, reject) => {
          this.platform.ready().then(
            () => {
              console.log('window ready om resolve location');
              window.resolveLocalFileSystemURL(imagePath,
                (fileEntry) => {
                  fileEntry.file(
                    (resFile) => {

                      console.log('resFile', resFile);

                      let reader = new window.FileReader();
                      reader.readAsArrayBuffer(resFile);

                      reader.onloadend = (evt: any) => {

                        var imgBlob: any = new Blob([evt.target.result], { type: 'image/jpeg' });
                        imgBlob.name = 'sample.jpg';
                        resolve(imgBlob);
                      };

                      reader.onerror = (evt: any) => {
                        this.utilsService.alert('Failed file read: ' + evt.toString());
                        reject(evt);
                      };
                    },
                    error => this.utilsService.alert(error)
                  );
                }
              );
            }
          );
        }
      );
    } else {
      return window
        .fetch(imagePath)
        .then(
          (response) => { return response.blob() }
        ).then(
          (blob) => { return blob }
        ).catch(
          error => this.utilsService.alert(error)
        );
    }
  }

  private uploadToFirebase(imageBlob: any, photoType: any) {
    console.log('imageBlob', imageBlob);

    let fileName = 'sample-' + new Date().getTime() + '.jpg';

    if (!!this.user[photoType].raw) {
      console.log('here raw');
      console.log(this.user.$key);
      console.log(this.user[photoType].raw);
      let photoRef = this.storage.ref(`userImages/${this.user.$key}/${this.user[photoType].raw}`);
      return photoRef
        .delete()
        .then(
          () => {
            console.log('deleted');
            return new Promise(
              (resolve, reject) => {
                let photoRef = this.storage.ref(`userImages/${this.user.$key}/${fileName}`);
                let uploadTask = photoRef.put(imageBlob);

                uploadTask.on('state_changed',
                  (snapshot) => console.log('snapshot progress ' + snapshot),
                  (error) => reject(uploadTask.snapshot),
                  () => {
                    let data = {
                      snapshot: uploadTask.snapshot,
                      fileName: fileName,
                      photoType: photoType
                    };
                    resolve(data)
                  }
                );
              }
            );
          },
          error => console.log('error', error)
        );
    } else {
      console.log('no raw');
      return new Promise(
        (resolve, reject) => {
          let photoRef = this.storage.ref(`userImages/${this.user.$key}/${fileName}`);
          let uploadTask = photoRef.put(imageBlob);

          uploadTask.on('state_changed',
            (snapshot) => console.log('snapshot progress ' + snapshot),
            (error) => reject(uploadTask.snapshot),
            () => {
              let data = {
                snapshot: uploadTask.snapshot,
                fileName: fileName
              };
              resolve(data)
            }
          );
        }
      );
    }
  }

  moveTo(page: string): void {

    let user = { user: this.user };

    switch (page) {
      case 'profile':
        this.nav.push('profile-edit', { user: user });
        break;
      case 'profile-card':
        this.nav.push('profile-card', { user: user });
        break;
      case 'profile-photos':
        this.nav.push('profile-photos', { user: user });
        break;
      case 'profile-reviews':
        this.nav.push('profile-reviews', { user: user });
        break;
      case 'profile-favorites':
        this.nav.push('profile-favorites', { user: user });
        break;
    }
  }

  takePhoto(photoType: string) {
    console.log('take photo', photoType);
    if (this.platform.is('cordova')) {
      this.utilsService
        .confirm('Take a photo or choose from Camera roll?', 'Take Photo', ['Take Photo', 'Camera Roll'])
        .then(
          (idx) => {
            console.log(idx);

            if (idx == 1)
              this.getPicture('CAMERA', photoType);
            else
              this.getPicture('PHOTOLIBRARY', photoType);
          }
        );
    }
  }

  goSearch() {
    setTimeout(
      () => this.nav.setRoot('search', { animate: true, direction: 'back' }),
      300
    );
  }

  onError(error: any) {
    // show error
    this.utilsService.alert(error);
    // hide loading
    this.utilsService.hideLoading();
  }

}
