import { Injectable } from '@angular/core';
import { Observable } from "rxjs";

declare var window: any;

@Injectable()
export class ToastService {

    constructor() {
    }

    show(message: string, duration: string = '3000', position: string = 'bottom'): Observable<any> {

        return new Observable(observer => {

            window.plugins.toast.showWithOptions(
                {
                    message: message,
                    duration: duration, // 2000 ms
                    position: position,
                    styling: {
                        opacity: 0.8, // 0.0 (transparent) to 1.0 (opaque). Default 0.8
                        backgroundColor: '#54C2FF',
                        textColor: '#FFFFFF', // Ditto. Default #FFFFFF
                        textSize: 13, // Default is approx. 13.
                        cornerRadius: 20, // minimum is 0 (square). iOS default 20, Android default 100
                        horizontalPadding: 16, // iOS default 16, Android default 50
                        verticalPadding: 12 // iOS default 12, Android default 30
                    }
                },
                (result) => {
                    observer.next(result);
                    observer.complete();
                },
                (err) => {
                    observer.error(err);
                    observer.complete();
                }
            );
        });
    }
}
