import { Injectable } from '@angular/core';

@Injectable()
export class AuthInfo {

    constructor(public $uid: string) {
    }

    isLoggedIn() {
        return !!this.$uid;
    }
}
