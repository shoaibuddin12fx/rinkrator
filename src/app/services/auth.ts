import {Injectable, Inject} from "@angular/core";
import {FirebaseAuth, FirebaseAuthState, FirebaseApp} from "angularfire2";
import { Observable, Subject } from "rxjs";

import { Facebook, TwitterConnect } from 'ionic-native';
//
// declare var firebase: any;
import * as firebase from 'firebase';

@Injectable()
export class Auth {

    firebase: any;

    constructor(private auth: FirebaseAuth, @Inject(FirebaseApp) firebaseApp: any) {
        this.firebase = firebaseApp;
    }

    login(email: string, password: string): Observable<FirebaseAuthState> {
        return this.fromAuthPromise(this.auth.login({email, password}));
    }

    signUp(email: string, password: string): Observable<FirebaseAuthState> {
        return this.fromAuthPromise(this.auth.createUser({email, password}));
    }

    reset(email: string): Observable<FirebaseAuthState> {
        return this.fromAuthPromise(this.firebase.auth().sendPasswordResetEmail(email));
    }

    facebookNativeLogin() {
        return this.fromAuthPromise(Facebook.login(['email']));
    }

    facebookFirebaseLogin(accessToken: string) {
        console.log('facebookFirebaseLogin: ', firebase);
        const facebookCredential = firebase.auth.FacebookAuthProvider.credential(accessToken);
        return this.fromAuthPromise(firebase.auth().signInWithCredential(facebookCredential));
    }

    twitterNativeLogin() {
        return this.fromAuthPromise(TwitterConnect.login());
    }

    twitterFirebaseLogin(token: string, secret: string) {
        const twitterCredential = firebase.auth.TwitterAuthProvider.credential(token, secret);
        return this.fromAuthPromise(firebase.auth().signInWithCredential(twitterCredential))
    }

    private fromAuthPromise(promise): Observable<any> {
        const subject = new Subject<any>();

        promise
            .then(
                res => {
                    subject.next(res);
                    subject.complete();
                },
                err => {
                    console.log(err);
                    subject.error(err);
                    subject.complete();
                }
            );

        return subject.asObservable();
    }

    logout() {
        this.auth.logout();
    }
}
